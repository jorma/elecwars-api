const isPromise = require('ispromise');

const Layer = require('express/lib/router/layer');

Layer.prototype.handle_error = function handle_error(error, req, res, next) { // eslint-disable-line camelcase,func-names
  const fn = this.handle;
  if (fn.length !== 4) {
    return next(error);
  }

  try {
    const handler = fn(error, req, res, next);
    if (isPromise(handler)) {
      handler.catch(next);
    }
  } catch (error2) {
    next(error2);
  }
};

Layer.prototype.handle_request = function handle_request(req, res, next) { // eslint-disable-line camelcase,func-names
  const fn = this.handle;

  if (fn.length > 3) {
    return next();
  }

  try {
    const handler = fn(req, res, next);
    if (isPromise(handler)) {
      handler.catch(next);
    }
  } catch (error) {
    next(error);
  }
};
