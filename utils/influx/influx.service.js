const config = require('config/environment');
const logger = require('utils/logger');

class Influx {
  constructor() {
    this.write = () => {};
    if (config.influx.enabled) {
      logger.info('InfluxDB [ENABLED]');
      const lib = require('mtrics-express');
      this.write = lib.write;

      return;
    }

    logger.warn('InfluxBD [DISABLED]');
  }

  get middleware() {
    if (config.influx.enabled) {
      const middleware = require('config/middlewares/influx');

      return middleware;
    }

    return (req, res, next) => next();
  }

  async write(...args) {
    try {
      return this.write(args);
    } catch (error) {}
  }
}

module.exports = new Influx();
