const mongoose = require('mongoose');

exports.init = (urlConnection, options) => mongoose.connect(urlConnection, options);
