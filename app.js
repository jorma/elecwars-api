require('app-module-path').addPath(__dirname);
require('utils/overrides'); // eslint-disable-line import/no-unassigned-import

const http = require('http');
const config = require('config/environment');
const app = require('config/express');
const logger = require('utils/logger');
const db = require('utils/db');

async function initialize() {
  try {
    const server = http.createServer(app);
    await db.init(config.mongo.uri, config.mongo.options);
    logger.info('Successfully connected to MongoDB Server');
    server.listen(config.port, () => {
      logger.info(`Server listening on ${config.port}, in ${config.env} mode`);
    });
  } catch (error) {
    logger.error('Something went wrong!');
    logger.error(error.message);
    await initialize();
  }
}

initialize();
