const express = require('express');
const bodyParser = require('body-parser');
const celebrate = require('celebrate');
const morgan = require('morgan');
const index = require('api/routes/index');
const accessControl = require('config/middlewares/access-control');
const handler = require('config/middlewares/error-handler');
const logger = require('utils/logger');
const influx = require('utils/influx');

const app = express();
app.enable('trust proxy');

const { middleware: influxMiddleware } = influx;
app.use(influxMiddleware);

app.use(morgan('tiny', { stream: logger.stream }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(accessControl);
app.use('/', index);

app.use(celebrate.errors());
app.use(handler);

module.exports = app;
