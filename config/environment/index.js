require('dotenv').config();
const path = require('path');
const { merge } = require('lodash');

process.env.NODE_ENV = process.env.NODE_ENV || 'development';
process.env.TZ = 'utc';

const env = process.env.NODE_ENV;
const tz = process.env.TZ;
const envConfig = require(`./${env}.js`);

const rootPath = path.normalize(`${__dirname}/../..`);

const all = {
  env,
  tz,
  root: rootPath,
  port: 9000,
  logger: {
    level: 'debug'
  },
  mongo: {
    uri: 'mongodb://localhost:27017/elecwars',
    options: {
      useCreateIndex: true,
      useNewUrlParser: true
    }
  },
  influx: {
    enabled: false,
    config: {
      measurement: {
        name: 'elecwars-api'
      },
      database: {
        host: 'localhost',
        port: 8086,
        name: 'elecwars'
      }
    }
  }
};

const processEnv = {
  port: process.env.PORT,
  logger: {
    level: process.env.LOG_LEVEL
  },
  mongo: {
    uri: process.env.MONGO_URI
  },
  influx: {
    enabled: process.env.INFLUX_ENABLED ? process.env.INFLUX_ENABLED === 'true' : undefined,
    config: {
      database: {
        host: process.env.INFLUX_HOST,
        port: process.env.INFLUX_PORT,
        name: process.env.INFLUX_DBNAME
      }
    }
  }
};

const config = merge(all, envConfig, processEnv);

module.exports = config;
