const devHandler = require('errorhandler');
const productionHandler = require('config/middlewares/error-handler/production-handler');
const config = require('config/environment');
const logger = require('utils/logger');

const handler = config.env === 'production' ? productionHandler : devHandler({ log: (error, str) => logger.error(str) });

module.exports = handler;
