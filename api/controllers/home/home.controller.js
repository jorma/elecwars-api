const packageJson = require('package.json');

class HomeController {
  index(req, res, next) {
    const returnObj = {
      name: packageJson.name,
      version: packageJson.version
    };

    res.status(200).send(returnObj);
  }
}

const controller = new HomeController();

module.exports = {
  index: controller.index.bind(controller)
};
