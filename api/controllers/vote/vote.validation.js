const { celebrate, Joi } = require('celebrate');

// POST /:faction body: { votes: Number }
// GET /summary dto: { pp: Number, psoe: Number, total: Number }

exports.vote = celebrate({
  params: {
    faction: Joi.string().required()
  },
  body: Joi.object().keys({
    votes: Joi.number().required().min(0),
    device: Joi.string()
  })
});

exports.user = celebrate({
  body: Joi.object().keys({
    device: Joi.string().required(),
    username: Joi.string().required()
  })
});
