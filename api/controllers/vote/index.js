const controller = require('api/controllers/vote/vote.controller');
const validation = require('api/controllers/vote/vote.validation');

module.exports = { controller, validation };
