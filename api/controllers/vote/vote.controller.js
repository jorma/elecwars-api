const Vote = require('api/models/vote');
const User = require('api/models/user');

class VoteController {
  constructor(model) {
    this._model = model;
  }

  async vote(req, res, next) {
    const { faction } = req.params;
    const { votes, device } = req.body;

    const vote = new this._model({ faction, score: votes, device });
    await vote.save();

    res.send({ stored: true });
  }

  async summary(req, res, next) {
    const pipeline = [{
      $group: {
        _id: {
          faction: '$faction'
        },
        faction: { $first: '$faction' },
        total: { $sum: '$score' }
      }
    }];

    const summary = await this._model.aggregate(pipeline);

    res.send(summary.reduce((group, unit) => {
      group[unit.faction] = unit.total;

      return group;
    }, {}));
  }

  async user(req, res, next) {
    const { device, username } = req.body;

    const user = new User({ device, username });
    await user.save();

    res.status(200).send({ stored: true });
  }
}

const controller = new VoteController(Vote);

module.exports = {
  vote: controller.vote.bind(controller),
  summary: controller.summary.bind(controller),
  user: controller.user.bind(controller)
};
