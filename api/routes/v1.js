const { Router } = require('express');
const { controller, validation } = require('api/controllers/vote');

const router = new Router();

router.post('/user', validation.user, controller.user);
router.post('/:faction', validation.vote, controller.vote);
router.get('/summary', controller.summary);

module.exports = router;
