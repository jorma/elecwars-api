const mongoose = require('mongoose');

const { Schema } = mongoose;
const UserSchema = new Schema({
  username: {
    type: String,
    index: true
  },
  device: {
    type: String,
    index: true
  }
}, { timestamps: true });

const Model = mongoose.model('User', UserSchema);

module.exports = Model;
