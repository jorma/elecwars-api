const mongoose = require('mongoose');

const { Schema } = mongoose;
const VoteSchema = new Schema({
  faction: {
    type: String,
    index: true
  },
  score: Number,
  device: {
    type: String,
    index: true
  }
}, { timestamps: true });

const Model = mongoose.model('Vote', VoteSchema);

module.exports = Model;
