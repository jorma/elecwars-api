FROM node:10-alpine AS builder
WORKDIR /opt/electwars-api
EXPOSE 9000
COPY package*.json ./
RUN npm install --production
COPY . .

FROM node:10-alpine AS image
WORKDIR /opt/electwars-api
COPY --from=builder /opt/electwars-api ./

CMD ["npm", "start"]
